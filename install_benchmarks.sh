#!/bin/bash
# Install script for PIRE benchmarks
# (c) 2019 Michael Bowcutt, Steven Xie

# install sysbench
sudo apt install make automake libtool pkg-config libaio-dev libmysqlclient-dev libssl-dev
git clone https://github.com/akopytov/sysbench.git
cd sysbench
./autogen.sh
./configure
make clean
make -j
sudo make install
cd ..

# install stress-ng
sudo apt install libaio-dev libapparmor-dev libattr1-dev libbsd-dev libcap-dev libgcrypt11-dev libipsec-mb-dev libkeyutils-dev libsctp-dev zlib1g-dev
git clone https://github.com/ColinIanKing/stress-ng.git
cd stress-ng
make clean
make
sudo make install
cd ..


