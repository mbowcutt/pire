#!/bin/bash

sudo apt install build-essential kernel-package fakeroot libncurses5-dev libssl-dev ccache
mkdir tresor-kernel
cd tresor-kernel
git clone -b linux-4.11.y git://git.kernel.org/pub/scm/linux/kernel/git/stable/linux-stable.git
wget http://www1.informatik.uni-erlangen.de/filepool/projects/tresor/tresor-patch-3.18.5_aesni
cd linux-stable
cp /boot/config-`uname -r` .config
yes '' | make oldconfig

patch -p1 < ../tresor-patch-3.18.5-aesni
make clean
make -j `getconf _NPROCESSORS_ONLN` deb-pkg LOCALVERSION=-custom

